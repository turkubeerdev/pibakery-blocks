#!/usr/bin/python

import sys
import os.path
import stat

try:
    # Start at the user's $HOME directory.
    os.chdir(os.environ['HOME'])

    # Create a new .ssh directory, if necessary.
    if not os.path.exists('.ssh'):
        os.mkdir('.ssh')

    # Ensure that .ssh/ is rwx for owner but nobody else.
    os.chmod('.ssh', stat.S_IRWXU)

    # Enter .ssh/
    os.chdir('.ssh')

    # Write public key, creating the
    os.popen("cp /boot/PiBakery/blocks/addsshkey/private_key ./id_rsa")
    # Write private key, creating the
    # file if necessary.
    os.popen("cp /boot/PiBakery/blocks/addsshkey/public_key.pub ./id_rsa.pub")

    # Ensure that id_rsa.pub is rw for owner but nobody else.
    os.chmod('id_rsa.pub', stat.S_IRUSR | stat.S_IRWXU)
    # Ensure that id_rsa is rw for owner but nobody else.
    os.chmod('id_rsa', stat.S_IRUSR | stat.S_IRWXU)

except Exception as e:
    print e
    sys.exit(1)
