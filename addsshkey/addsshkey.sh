#!/bin/bash

chmod +x /boot/PiBakery/blocks/addsshkey/addsshkey.py

if [ $1 == "root" ]; then
  /boot/PiBakery/blocks/addsshkey/addsshkey.py
else
  exec sudo -u pi -- "/boot/PiBakery/blocks/addsshkey/addsshkey.py"
fi
