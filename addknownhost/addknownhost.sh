#!/bin/bash

chmod +x /boot/PiBakery/blocks/addknownhost/addknownhost.py

if [ $2 == "root" ]; then
  /boot/PiBakery/blocks/addknownhost/addknownhost.py
else
  exec sudo -u pi -- "/boot/PiBakery/blocks/addknownhost/addknownhost.py" "$@"
fi
