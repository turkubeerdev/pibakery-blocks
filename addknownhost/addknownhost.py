#!/usr/bin/python

import sys
import os.path
import stat
from subprocess import check_output


try:
    # Start at the user's $HOME directory.
    os.chdir(os.environ['HOME'])

    # Create a new .ssh directory, if necessary.
    if not os.path.exists('.ssh'):
        os.mkdir('.ssh')

    # Ensure that .ssh/ is rwx for owner but nobody else.
    os.chmod('.ssh', stat.S_IRWXU)

    # Enter .ssh/
    os.chdir('.ssh')

    # Remove a host key, if necessary
    os.popen('ssh-keygen -R ' + sys.argv[1])

    # Get host key from host
    host_key = check_output(['ssh-keyscan', sys.argv[1]])

    # Create known_hosts file, if necessary
    # Append the host key to known_hosts, creating the
    # file if necessary.
    with open('known_hosts', 'a') as f:
        f.write(host_key + '\n')
        f.close()


    # Ensure that known_hosts is rw for owner but nobody else.
    os.chmod('known_hosts', stat.S_IRUSR | stat.S_IRWXU)

except Exception as e:
    print e
    sys.exit(1)
