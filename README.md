# **Manual for using PiBakery** #

## **Download** ##
* You can download PiBakery from ***[here](http://www.pibakery.org/download.html)*** 

## **Setup for Building Energy Project** ##
* You can import blocks and .xml file or add blocks and manually add information into blocks. 
 
* To import block, first, you need import the folders which include Pibakery block.
* * You can drag and drop. If drag and drop do not work, you should use (Ctrl Shift +) to import that folder (It's a fault of PiBakery).
* Next, you can import the .xml file by using "Import" button or you can manually add information to blocks.
* After all, you choose "Write" to write to SD card.

## **Example** ##

**Setup for RPI to send IP address and enable all GPIO pins for 1-wire devices.**

* Importing the folder - ***addGitrepo*** folder
* Importing .xml file or selecting needed blocks 
![27_02.JPG](https://bitbucket.org/repo/MrGejq7/images/4265637540-27_02.JPG)

* Then you can Write to SD card 

## ***Note***: ##
*  Git's url: must be in correct form. It could be github.com/your_git/git_repository or xx.xx.xxx.xxx/git/your_project_name (Redmine)
*  Do not include "https://" in Git's url.
*  Git's folder must be specified. It can be project_name on Redmine or git_repository on Github
*  **On the first time**, setting up wifi for RPI, it must connect to internet by using Ethernet cable.
*  You can adding more blocks if you want. For example, you want to add your ssh key to RPI. Then you need this: 
![092802.JPG](https://bitbucket.org/repo/MrGejq7/images/102790803-092802.JPG)


*  You must consider that block is done once or repeat every time RPI is rebooted.
*  To correct **TimeZone** (Finland), you need add blocks to run those commands:
* * sudo unlink /etc/localtime
* * sudo ln -s /usr/share/zoneinfo/Europe/Helsinki /etc/localtime